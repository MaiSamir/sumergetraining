import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.groupingBy;

public class task {

    public static void main(String[] args) {
        List<Employee> allEmployees = Arrays.asList(
                new Employee("mai", "ASWE", "011000000"),
                new Employee("rana", "ASWE", "011111111"),
                new Employee("SAM", "SE", "110100101010"),
                new Employee("Nancy", "SE", "0122222222"),
                new Employee("Nancy", "NV", "0122222222")
        );

        countEmps foo = c-> {
            if(c>=2){
                System.out.println("Count: "+ c);
            }
            else{
                throw new IOException("Employees less than 2");
            }

        };

        //Group by title
        allEmployees.stream()
                .collect(groupingBy(Employee::getTitle))
                .forEach((title, employees) -> {
                    
                        try {
                            System.out.println("Title: "+ title);
                            foo.count(employees.size());
                            employees.forEach( e->System.out.println("name: "+e.name+" mobile: "+e.mobile));

                        } catch (IOException e) {
                            System.out.println(e.getMessage());
                        }



                });

    }
}
