import java.io.IOException;

@FunctionalInterface
public interface countEmps{
    void count(int c) throws IOException;
}